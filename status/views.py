from django.shortcuts import render

from django.http import HttpResponse
import json

def status(request):
    status_message = {'status':'Birddog is flying, captain!'}
    message_json = json.dumps(status_message)
    return HttpResponse(message_json)
