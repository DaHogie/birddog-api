from django.shortcuts import render
import os
import json
import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors
from google.oauth2 import service_account
import os.path
from os import path
from apiclient.errors import HttpError
from django.http import JsonResponse


scopes = ['https://www.googleapis.com/auth/youtube.force-ssl']

def youtube_api(request):

    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    dirname = os.path.dirname(__file__)


    api_service_name = 'youtube'
    api_version = 'v3'
    SERVICE_ACCOUNT_FILE = os.path.join(dirname, 'add relative path to service account file')

    #create an API client
    credentials = service_account.Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE,scopes=scopes)
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, credentials=credentials)

    #variables to store the channel info.
    response_channel = {}
    response_videos = {}
    data = {}
    data['videos'] = []
    page_token = None
    max = 50

    #Try exception checks whether the google query quota has been reached.
    try:
            #while loop will return video data from the youtube channel and store it
            #in a dictionary.
            while True:
                response_channel = youtube.search().list(
                    part = 'snippet',
                    channelId = 'UC-5Ee7bLDbEaL23kN9at23A',
                    order = 'date',
                    pageToken = page_token,
                    maxResults = max,
                    type = 'video'
                ).execute()
                #this for loop will store the data we need from the dictionary to a list append
                #uses the video id to retrieve the videos description.
                for item in response_channel['items']:
                    new_video = {}
                    new_video['videoId'] = item['id']['videoId']
                    new_video['title'] = item['snippet']['title']
                    response_videos = youtube.videos().list(
                        part = 'snippet',
                        id = item['id']['videoId']
                    ).execute()
                    new_video['description'] = response_videos['items'][0]['snippet']['description']
                    data['videos'].append(new_video)
                #stores the nextPageToken to a string to be used in the get request
                page_token = response_channel.get('nextPageToken')

                #the while loop will end if there is no nextPageToken
                if not page_token:
                    break

            #will open the file that is used to store the video info and write our data to it.
            with open(os.path.join(dirname, 'channel_info.json'),'w') as outfile:
                json.dump(data,outfile, indent = 4)

    #Exception will print an error message if the quota has been reached.
    except HttpError:
        print('Daily Quota has been reached.')

    return JsonResponse(data)
